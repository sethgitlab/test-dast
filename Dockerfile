FROM nginx:1.17.6

COPY webserver/ /usr/share/nginx/html/
COPY webserver/nginx.conf /etc/nginx/conf.d/default.conf
